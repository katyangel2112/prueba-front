import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/Home',
    name: 'Home',
    component: Home,
    meta: {
      auth: true,
    }
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

router.beforeEach((to, from, next) => {
  let usuario = localStorage.getItem("access_token");
  let autorizacion = to.matched.some((record) => record.meta.auth);

  if (autorizacion && !usuario) {
    next("/");
  } else if (to.name === 'Login' && usuario) {
    next("/home");
  } else {
    next();
  }
});


export default router
